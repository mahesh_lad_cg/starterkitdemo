import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test(
    "Check if keys are missing from en.json(default language) files",
    () async {
      final String enTranslationContent =
          File('languages/en.json').readAsStringSync();
      final Map<String, dynamic> enJson = json.decode(enTranslationContent);
      final Directory dir = Directory('languages/');
      final List<FileSystemEntity> entities = await dir.list().toList();
      final Iterable<File> files = entities.whereType<File>();
      bool isKeyMissing = false;
      for (final File file in files) {
        if (!file.path.contains("en.json")) {
          final String translation = file.readAsStringSync();
          final Map<String, dynamic> otherLanguageJson =
              json.decode(translation);
          final Iterable<String> diff = otherLanguageJson.keys
              .where((String element) => !enJson.keys.contains(element));
          if (diff.isNotEmpty) {
            debugPrint(
              "Below key's are missing from en.json which are available in ${file.path} \n $diff",
            );
            isKeyMissing = true;
          }
        }
      }
      expect(isKeyMissing, false);
    },
  );

  //TODO:
  test(
    "Check if language file is missing",
        () async {
      final String enTranslationContent =
      File('languages/en.json').readAsStringSync();
      final Map<String, dynamic> enJson = json.decode(enTranslationContent);
      final Directory dir = Directory('languages/');
      final List<FileSystemEntity> entities = await dir.list().toList();
      final Iterable<File> files = entities.whereType<File>();
      bool isKeyMissing = false;
      for (final File file in files) {

      }
      expect(isKeyMissing, false);
    },
  );
}
