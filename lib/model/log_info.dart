class InfoEvent {
  String message;

  InfoEvent(this.message);

  @override
  String toString() {
    return message;
  }
}

class HasInfo {
  List<InfoEvent> getInfo() {
    return <InfoEvent>[];
  }
}
