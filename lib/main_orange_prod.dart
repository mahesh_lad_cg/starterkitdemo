import 'view/main/main.dart';
import 'flavors/orange/orange_config_prod.dart';

void main() {
  initializeMain(OrangeConfigProd());
}
