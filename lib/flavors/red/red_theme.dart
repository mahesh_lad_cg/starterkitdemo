import 'package:flutter/material.dart';

import '../../themes/abstract_theme.dart';

class RedTheme extends AbstractTheme {
  @override
  Color? darkScaffoldBackgroundColor = Colors.black;

  @override
  Color? lightScaffoldBackgroundColor = Colors.white;

  @override
  Color? primaryColor = const Color(0xFFBC1714);

  @override
  Color? darkMenuItemBoxShadow = Colors.white;

  @override
  Color? lightMenuItemBoxShadow = Colors.grey;
}
