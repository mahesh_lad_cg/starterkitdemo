import '../../configs/config_uat.dart';
import '../../themes/abstract_theme.dart';
import 'red_theme.dart';

class RedConfigUat extends ConfigUat {
  @override
  AbstractTheme abstractTheme = RedTheme();

  @override
  String appName="Red uat";

  @override
  String baseURL="";
}
