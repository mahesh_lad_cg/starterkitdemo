import '../../configs/config_prod.dart';
import '../../themes/abstract_theme.dart';
import 'red_theme.dart';

class RedConfigProd extends ConfigProd {
  @override
  AbstractTheme abstractTheme = RedTheme();

  @override
  String appName="Red prod";

  @override
  String baseURL="";
}
