import '../../configs/config_prod.dart';
import '../../themes/abstract_theme.dart';
import 'orange_theme.dart';

class OrangeConfigProd extends ConfigProd {
  @override
  AbstractTheme abstractTheme = OrangeTheme();

  @override
  String appName="Orange prod";

  @override
  String baseURL="";
}
