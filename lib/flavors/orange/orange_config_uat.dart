import '../../configs/config_uat.dart';
import '../../themes/abstract_theme.dart';
import 'orange_theme.dart';

class OrangeConfigUat extends ConfigUat {
  @override
  AbstractTheme abstractTheme = OrangeTheme();

  @override
  String appName="Orange uat";

  @override
  String baseURL="";
}
