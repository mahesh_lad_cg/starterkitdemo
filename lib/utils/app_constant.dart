class AppConstant {
  static const _AppLifeCycle appLifeCycle = _AppLifeCycle();
  static const _NativeCall nativeCall = _NativeCall();
  static const _Network network = _Network();
  static const _Database database = _Database();
}

class _AppLifeCycle {
  final String appStarted = "APP_STARTED";
  final String appResumed = "APP_RESUMED";
  final String appPaused = "APP_PAUSED";
  final String appInactive = "APP_INACTIVE";
  final String appDetached = "APP_DETACHED";

  const _AppLifeCycle();
}

class _NativeCall {
  final String channelName = "APP_NATIVE_CALL_CHANNEL";
  final String yourNativeMethodName = "YOUR_NATIVE_METHOD_NAME";
  final String callFromNativeToFlutter = "YOUR_CALL_FROM_NATIVE_TO_FLUTTER";

  const _NativeCall();
}

class _Network {
  final int connectionTimeOut = 30000;
  final int receiveTimeOut = 30000;
  final String responseTime = 'response_time';

  const _Network();
}

class _Database {
  final String databaseName = 'YOUR_DATABASE_NAME';
  final int versionCode = 1;

  const _Database();
}
