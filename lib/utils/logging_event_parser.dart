import 'package:flutter_bloc/flutter_bloc.dart';

import '../model/log_info.dart';

class LoggingEventParser {
  List<dynamic>? parseStateToEvent(Transition<dynamic, dynamic>? transition) {
    final dynamic state = transition?.nextState;
    if (state is HasInfo) {
      return state.getInfo();
    } else {
      return null;
    }
  }
}
