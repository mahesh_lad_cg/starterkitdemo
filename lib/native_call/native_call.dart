import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../utils/app_constant.dart';

class NativeCall extends InheritedWidget {
  final MethodChannel _channel =
      MethodChannel(AppConstant.nativeCall.channelName);

  NativeCall({
    Key? key,
    required Widget child,
  }) : super(
          key: key,
          child: child,
        );

  static NativeCall of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<NativeCall>()!;
  }

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) {
    return true;
  }

  //Call this method as following from the app ; NativeCall.of(context).callNativeMethod();
  void callNativeMethod() {
    _channel.setMethodCallHandler(receiveInfo);
    _channel.invokeMethod(AppConstant.nativeCall.yourNativeMethodName);
  }

  Future<void> receiveInfo(MethodCall call) async {
    if (call.method == AppConstant.nativeCall.callFromNativeToFlutter) {
      debugPrint(AppConstant.nativeCall.callFromNativeToFlutter);
    } else {
      debugPrint("Not implemented");
    }
  }
}
