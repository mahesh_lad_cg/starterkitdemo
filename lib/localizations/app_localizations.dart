import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalizations {
  final Locale _locale;
  Map<String, String>? _localizedStrings;
  Map<String, String>? _enLocalizedStrings;

  static const LocalizationsDelegate<AppLocalizations> delegate =
      _AppLocalizationsDelegate();
  static const List<Locale> supportedLanguages = [
    Locale('en', 'US'), //whichever is first item will be default language
    Locale('de', 'DE'),
  ];

  AppLocalizations(this._locale);

  static AppLocalizations? of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  Future<bool> load() async {
    String jsonString =
        await rootBundle.loadString('languages/${_locale.languageCode}.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _localizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });

    if(_locale.languageCode!="en") {
      await _loadEnglishAsDefaultLocal();
    }

    return true;
  }

  String translate(String key) {
    return _localizedStrings?[key] ?? _enLocalizedStrings?[key] ?? key;
  }

  Future<void> _loadEnglishAsDefaultLocal() async {
    String jsonString =
        await rootBundle.loadString('languages/en.json');
    Map<String, dynamic> jsonMap = json.decode(jsonString);

    _enLocalizedStrings = jsonMap.map((key, value) {
      return MapEntry(key, value.toString());
    });
  }
}

class _AppLocalizationsDelegate
    extends LocalizationsDelegate<AppLocalizations> {
  const _AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    List<String> supportedLanguages = [];
    for (Locale supportedLocal in AppLocalizations.supportedLanguages) {
      supportedLanguages.add(supportedLocal.languageCode);
    }

    return supportedLanguages.contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) async {
    AppLocalizations localizations = AppLocalizations(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(_AppLocalizationsDelegate old) => false;
}
