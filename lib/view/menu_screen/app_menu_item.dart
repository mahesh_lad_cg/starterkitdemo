import 'package:flutter/material.dart';
import 'package:starter_kit_demo/configs/app_config.dart';
import 'package:starter_kit_demo/utils/util.dart';
import '../../navigation/app_navigation.dart';

class AppMenuItem extends StatelessWidget {
  final int count;
  final String title;
  final double imageRadius;

  const AppMenuItem({
    Key? key,
    required this.count,
    required this.title,
    required this.imageRadius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        AppNavigator.push(
          context: context,
          routeName: AppNavigator.homeRoute,
          argument: title,
        );
      },
      child: Stack(
        children: [
          buildMenuDetails(context),
          buildMenuAction(),
          buildMenuCircularNetworkImage(),
        ],
      ),
    );
  }

  Positioned buildMenuCircularNetworkImage() {
    return const Positioned.fill(
      child: Align(
        alignment: Alignment.centerLeft,
        child: CircleAvatar(
          radius: 45,
          backgroundImage: NetworkImage('https://picsum.photos/90'),
          backgroundColor: Colors.transparent,
        ),
      ),
    );
  }

  Positioned buildMenuAction() {
    return Positioned.fill(
      child: Align(
        alignment: Alignment.centerRight,
        child: Container(
          padding: const EdgeInsets.all(5),
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
              ),
            ],
          ),
          child: const Icon(
            Icons.arrow_forward_ios_rounded,
          ),
        ),
      ),
    );
  }

  Container buildMenuDetails(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 75),
      margin: const EdgeInsets.only(right: 10, left: 45),
      height: 100,
      width: double.infinity,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: isDarkMode(context)
                ? Colors.transparent
                : AppConfig.of(context).abstractTheme.lightMenuItemBoxShadow!,
            blurRadius: 5.0,
          ),
        ],
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(10.0),
          bottomRight: Radius.circular(10.0),
          topLeft: Radius.circular(30.0),
          bottomLeft: Radius.circular(30.0),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title,
            style: const TextStyle(
              fontSize: 24,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            "$count item",
            style: const TextStyle(
              fontSize: 12,
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
