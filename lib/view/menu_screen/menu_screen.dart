import 'package:flutter/material.dart';

import 'app_bar_widget.dart';
import 'menu_widget.dart';
import 'search_bar_widget.dart';

class MenuScreen extends StatelessWidget {
  const MenuScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const AppBarWidget(),
      body: Column(
        children: const [
          SearchBarWidget(),
          MenuWidget(),
        ],
      ),
    );
  }
}
