import 'package:flutter/material.dart';
import 'package:starter_kit_demo/localizations/app_localizations.dart';

import 'app_menu_item.dart';

class MenuWidget extends StatelessWidget {
  const MenuWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          buildSideBar(context),
          buildMenuList(context),
        ],
      ),
    );
  }

  Align buildMenuList(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: const EdgeInsets.only(
          left: 20,
          top: 100,
          right: 20,
        ),
        child: ListView(
          children: [
            AppMenuItem(
              title: AppLocalizations.of(context)!.translate('food'),
              count: 120,
              imageRadius: 45,
            ),
            const SizedBox(
              height: 20,
            ),
            AppMenuItem(
              title: AppLocalizations.of(context)!.translate('beverages'),
              count: 100,
              imageRadius: 8,
            ),
          ],
        ),
      ),
    );
  }

  Align buildSideBar(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: const EdgeInsets.only(
          top: 50,
          bottom: 50,
        ),
        height: double.infinity,
        width: 120,
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(40.0),
            bottomRight: Radius.circular(40.0),
            topLeft: Radius.circular(0.0),
            bottomLeft: Radius.circular(0.0),
          ),
        ),
      ),
    );
  }
}
