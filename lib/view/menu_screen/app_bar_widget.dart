import 'package:flutter/material.dart';
import 'package:starter_kit_demo/configs/app_config.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  const AppBarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(
        AppConfig.of(context).appName,
        style: const TextStyle(),
      ),
      elevation: 0,
      actions: [
        IconButton(
          icon: const Icon(
            Icons.shopping_cart,
          ),
          onPressed: () {},
        ),
      ],
    );
  }
}
