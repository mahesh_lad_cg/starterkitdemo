import 'package:flutter/material.dart';
import '../../repositories/api/food_api_repository.dart';

import '../../configs/abstract_config.dart';
import '../../network/network_client.dart';
import '../../network/network_hadler.dart';

class Provider extends StatelessWidget {
  final Widget child;
  final AbstractConfig abstractConfig;

  const Provider({
    Key? key,
    required this.child,
    required this.abstractConfig,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NetworkHandler networkHandler = NetworkHandler(
      abstractConfig: abstractConfig,
      isDebugMode: abstractConfig.isDebuggable,
    );
    final ApiClient apiClient = networkHandler.getClient();
    FoodApiRepository(apiClient,abstractConfig);
    return child;
  }
}
