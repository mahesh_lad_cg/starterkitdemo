import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import '../../configs/abstract_config.dart';
import '../../configs/app_config.dart';
import '../../localizations/app_localizations.dart';
import '../../model/log_info.dart';
import '../../native_call/native_call.dart';
import '../../navigation/app_navigation.dart';
import '../../services/logging_service.dart';
import 'app_ife_cycle_widget.dart';
import 'multi_bloc.dart';
import 'provider.dart';

void initializeMain(AbstractConfig abstractConfig) {
  runZonedGuarded(
    () {
      WidgetsFlutterBinding.ensureInitialized();
      return BlocOverrides.runZoned(
        () => runApp(
          AppConfig(
            abstractConfig: abstractConfig,
            child: Provider(
              abstractConfig:abstractConfig,
              child: MultipleBlocProvider(
                child: NativeCall(
                  child: const AppLifeCycleWidget(
                    child: Main(),
                  ),
                ),
              ),
            ),
          ),
        ),
        blocObserver: AppBlocObserver(),
      );
    },
    (Object error, StackTrace stack) {
      LoggingService.instance.logDartError(error, stack);
    },
  );
}

class AppBlocObserver extends BlocObserver {
  AppBlocObserver();

  @override
  void onChange(BlocBase bloc, Change change) {
    super.onChange(bloc, change);
    debugPrint('${bloc.runtimeType} $change');
  }

  @override
  void onTransition(
    Bloc<dynamic, dynamic> bloc,
    Transition<dynamic, dynamic> transition,
  ) {
    if (transition.nextState is HasInfo) {
      LoggingService.instance.logInfo(transition);
    }
    debugPrint('${bloc.runtimeType} $transition');
    super.onTransition(bloc, transition);
  }
}

class Main extends StatelessWidget {
  const Main({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AbstractConfig abstractConfig = AppConfig.of(context);
    return MaterialApp(
      title: abstractConfig.appName,
      debugShowCheckedModeBanner: false,
      theme: abstractConfig.abstractTheme.getAppLightTheme(),
      darkTheme: abstractConfig.abstractTheme.getAppDarkTheme(),
      //themeMode: ThemeMode.dark, //use to set default theme
      onGenerateRoute: AppNavigator.generateRoute,
      supportedLocales: AppLocalizations.supportedLanguages,
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale?.languageCode &&
              supportedLocale.countryCode == locale?.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first; //considered en as default language
      },
    );
  }
}
