import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/app_life_cycle/app_life_cycle_bloc.dart';

class AppLifeCycleWidget extends StatefulWidget {
  final Widget child;

  const AppLifeCycleWidget({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  State<AppLifeCycleWidget> createState() => _AppLifeCycleWidgetState();
}

class _AppLifeCycleWidgetState extends State<AppLifeCycleWidget>
    with WidgetsBindingObserver {
  AppLifeCycleBloc _appLifeCycleBloc=AppLifeCycleBloc();

  @override
  void initState() {
    WidgetsBinding.instance!.addObserver(this);
    _appLifeCycleBloc.add(AppLifecycleStarted());
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _appLifeCycleBloc = context.read<AppLifeCycleBloc>();
    return widget.child;
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.resumed:
        _appLifeCycleBloc.add(AppLifecycleResumed());
        break;
      case AppLifecycleState.inactive:
        _appLifeCycleBloc.add(AppLifeCycleInActivated());
        break;
      case AppLifecycleState.paused:
        _appLifeCycleBloc.add(AppLifeCyclePaused());
        break;
      case AppLifecycleState.detached:
        _appLifeCycleBloc.add(AppLifeCycleDetached());
        break;
    }
  }
}