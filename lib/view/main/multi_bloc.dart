import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/app_life_cycle/app_life_cycle_bloc.dart';

class MultipleBlocProvider extends StatelessWidget {
  final Widget child;

  const MultipleBlocProvider({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: <BlocProvider<dynamic>>[
        BlocProvider<AppLifeCycleBloc>(
          create: (BuildContext context) => AppLifeCycleBloc(),
        ),
      ],
      child: child,
    );
  }
}
