import 'package:flutter/material.dart';

class ErrorScreen extends StatelessWidget {
  final String name;

  const ErrorScreen(
    this.name, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          'No route defined for $name',
        ),
      ),
    );
  }
}
