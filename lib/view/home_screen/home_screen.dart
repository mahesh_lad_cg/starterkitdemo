import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  final String menuName;

  const HomeScreen({
    Key? key,
    required this.menuName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text(
          menuName,
        ),
      ),
    );
  }
}
