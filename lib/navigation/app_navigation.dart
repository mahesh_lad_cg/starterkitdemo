import 'package:flutter/material.dart';
import '../view/home_screen/home_screen.dart';
import '../view/menu_screen/menu_screen.dart';
import '../view/route_not_found_screen/route_not_found_screen.dart';

class AppNavigator {
  static const menuRoute = "/";
  static const homeRoute = "/home";

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case menuRoute:
        return MaterialPageRoute(
          builder: (_) => const MenuScreen(),
        );
      case homeRoute:
        return MaterialPageRoute(
          builder: (_) => HomeScreen(menuName : settings.arguments as String),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => ErrorScreen(
            settings.name ?? "Route not found!",
          ),
        );
    }
  }

  static push({
    required BuildContext context,
    required String routeName,
    Object? argument,
  }) {
    Navigator.pushNamed(
      context,
      routeName,
      arguments: argument,
    );
  }

  static pop({
    required BuildContext context,
  }) {
    Navigator.pop(context);
  }
}
