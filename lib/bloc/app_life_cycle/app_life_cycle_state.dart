part of 'app_life_cycle_bloc.dart';

abstract class AppLifeCycleState extends BaseState {
  const AppLifeCycleState();
}

class AppLifeCycleInitial extends AppLifeCycleState {
  @override
  List<Object> get props => [];
}

class AppLifecycleStart extends AppLifeCycleState implements HasInfo {
  @override
  List<InfoEvent> getInfo() {
    return [InfoEvent(AppConstant.appLifeCycle.appStarted)];
  }
}

class AppLifecycleResume extends AppLifeCycleState implements HasInfo {
  @override
  List<InfoEvent> getInfo() {
    return [InfoEvent(AppConstant.appLifeCycle.appResumed)];
  }
}

class AppLifeCycleInActive extends AppLifeCycleState implements HasInfo {
  @override
  List<InfoEvent> getInfo() {
    return [InfoEvent(AppConstant.appLifeCycle.appInactive)];
  }
}

class AppLifeCyclePause extends AppLifeCycleState implements HasInfo {
  @override
  List<InfoEvent> getInfo() {
    return [InfoEvent(AppConstant.appLifeCycle.appPaused)];
  }
}

class AppLifeCycleDetach extends AppLifeCycleState implements HasInfo {
  @override
  List<InfoEvent> getInfo() {
    return [InfoEvent(AppConstant.appLifeCycle.appDetached)];
  }
}
