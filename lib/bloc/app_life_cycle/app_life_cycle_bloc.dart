import 'package:flutter_bloc/flutter_bloc.dart';
import '../../model/log_info.dart';
import '../../utils/app_constant.dart';

import '../base/base_bloc.dart';

part 'app_life_cycle_event.dart';

part 'app_life_cycle_state.dart';

class AppLifeCycleBloc extends BaseBloc<AppLifeCycleEvent, AppLifeCycleState> {


  AppLifeCycleBloc() : super(AppLifeCycleInitial()) {
    on<AppLifecycleStarted>(_appLifecycleStarted);
    on<AppLifecycleResumed>(_appLifecycleResumed);
    on<AppLifeCycleInActivated>(_appLifeCycleInactive);
    on<AppLifeCyclePaused>(_appLifeCyclePaused);
    on<AppLifeCycleDetached>(_appLifeCycleDetached);
  }

  Future<void> _appLifecycleStarted(
    AppLifeCycleEvent event,
    Emitter<AppLifeCycleState> emit,
  ) async {
    emit(AppLifecycleStart());
  }

  Future<void> _appLifecycleResumed(
    AppLifeCycleEvent event,
    Emitter<AppLifeCycleState> emit,
  ) async {
    emit(AppLifecycleResume());
  }

  Future<void> _appLifeCycleInactive(
    AppLifeCycleEvent event,
    Emitter<AppLifeCycleState> emit,
  ) async {
    emit(AppLifeCycleInActive());
  }

  Future<void> _appLifeCyclePaused(
    AppLifeCycleEvent event,
    Emitter<AppLifeCycleState> emit,
  ) async {
    emit(AppLifeCyclePause());
  }

  Future<void> _appLifeCycleDetached(
    AppLifeCycleEvent event,
    Emitter<AppLifeCycleState> emit,
  ) async {
    emit(AppLifeCycleDetach());
  }
}
