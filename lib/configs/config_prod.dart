import 'abstract_config.dart';

abstract class ConfigProd extends AbstractConfig{
  @override
  bool isDebuggable = false;

  @override
  Environment environment=Environment.prod;
}