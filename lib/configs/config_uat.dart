import 'abstract_config.dart';

abstract class ConfigUat extends AbstractConfig {
  @override
  bool isDebuggable = true;

  @override
  Environment environment = Environment.uat;
}
