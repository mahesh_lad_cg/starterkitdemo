import '../themes/abstract_theme.dart';

enum Environment{
  uat,
  prod
}

abstract class AbstractConfig {
  abstract AbstractTheme abstractTheme;
  abstract String appName;
  abstract Environment environment;
  abstract bool isDebuggable;
  abstract String baseURL;
}