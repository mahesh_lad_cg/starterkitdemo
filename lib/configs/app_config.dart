import 'package:flutter/material.dart';

import '../services/logging_service.dart';
import 'abstract_config.dart';

class AppConfig extends InheritedWidget {
  final AbstractConfig abstractConfig;

  AppConfig({
    Key? key,
    required Widget child,
    required this.abstractConfig,
  }) : super(
          key: key,
          child: child,
        ) {
    debugPrint(
      "Starting [appName=${abstractConfig.appName}]"
      " with config ${abstractConfig.environment.name}",
    );
    initFlutterErrorLogging();
  }

  @override
  bool updateShouldNotify(covariant InheritedWidget oldWidget) => false;

  static AbstractConfig of(BuildContext context) {
    return context.findAncestorWidgetOfExactType<AppConfig>()!.abstractConfig;
  }

  void initFlutterErrorLogging() {
    FlutterError.onError = (FlutterErrorDetails details) {
      LoggingService.instance.logFlutterError(details);
    };
  }
}
