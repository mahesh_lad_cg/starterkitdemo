import 'package:flutter/material.dart';

abstract class DarkTheme {
  abstract Color? darkScaffoldBackgroundColor;
  abstract Color? darkMenuItemBoxShadow;
}
