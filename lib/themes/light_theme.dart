import 'package:flutter/material.dart';

abstract class LightTheme {
  abstract Color? lightScaffoldBackgroundColor;
  abstract Color? lightMenuItemBoxShadow;
}
