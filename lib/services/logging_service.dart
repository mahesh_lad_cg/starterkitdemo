import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../utils/logging_event_parser.dart';

class LoggingService {

  LoggingService._privateConstructor();

  static final LoggingService _instance = LoggingService._privateConstructor();

  //we can use this constructor to pass values
  factory LoggingService(){
    return _instance;
  }

  //OR if we do not need to pass values we can use this field
  static LoggingService get instance {
    return _instance;
  }

  void logFlutterError(FlutterErrorDetails details){
    debugPrintStack(
      stackTrace: details.stack,
      label: details.exception.toString(),
    );
  }

  void logDartError(Object error, StackTrace stackTrace){
    debugPrintStack(
      stackTrace: stackTrace,
      label: error.toString(),
    );
  }

  void logInfo(Transition<dynamic, dynamic> transition){
    try {
      final List<dynamic>? info = LoggingEventParser().parseStateToEvent(transition);
      if (info != null && info.isNotEmpty) {
        for (var element in info) {
          debugPrint(element.toString());
        }
      }
    } catch (e) {
      logError('Logging_service.dart:logInfo\n$e');
    }
  }

  void logError(String s) {
    debugPrint("Error : $s");
  }

}