import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

import '../../configs/abstract_config.dart';
import '../../network/network_client.dart';
import '../../utils/app_constant.dart';

const String jsonContentType = 'application/json';

abstract class BaseApiRepository {
  ApiClient apiClient;
  AbstractConfig config;

  BaseApiRepository(this.apiClient, this.config);

  Future<T> makeRequest<T>(Future<T> Function() apiRequest) async {
    Stopwatch? stopwatch = Stopwatch()..start();
    final T res = await apiRequest.call();
    try {
      final int elapsedMilliSeconds = stopwatch.elapsedMilliseconds;
      if (res is Response) {
        res.extra.addAll(
          <String, dynamic>{
            AppConstant.network.responseTime: elapsedMilliSeconds
          },
        );
      }
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      if (stopwatch.isRunning) stopwatch.stop();
      stopwatch = null;
    }
    return res;
  }
}
