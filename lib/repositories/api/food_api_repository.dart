import '../../configs/abstract_config.dart';
import '../../network/network_client.dart';
import 'base_api_repository.dart';

class FoodApiRepository extends BaseApiRepository {
  static FoodApiRepository? _instance;

  factory FoodApiRepository(
    ApiClient apiClient,
    AbstractConfig config,
  ) {
    _instance ??= FoodApiRepository._internal(apiClient, config);
    return _instance!;
  }

  //OR if we do not need to pass values we can use this field
  static FoodApiRepository get instance {
    return _instance!;
  }

  FoodApiRepository._internal(
    ApiClient apiClient,
    AbstractConfig config,
  ) : super(apiClient, config);


}
