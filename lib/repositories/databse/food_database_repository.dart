import 'base_database_repository.dart';

class FoodDatabaseRepository extends BaseDatabaseRepository {

  FoodDatabaseRepository._privateConstructor();

  static final FoodDatabaseRepository _instance =
      FoodDatabaseRepository._privateConstructor();

  //we can use this constructor to pass values
  factory FoodDatabaseRepository() {
    return _instance;
  }

  //OR if we do not need to pass values we can use this field
  static FoodDatabaseRepository get instance {
    return _instance;
  }

  void insertFood(){
    //getDb().then((Database database) => database.insert(table, values));
  }
}
