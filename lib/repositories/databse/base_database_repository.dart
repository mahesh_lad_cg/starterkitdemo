import 'package:sqflite/sqflite.dart';
import 'app_database.dart';

class BaseDatabaseRepository {
  Future<Database> getDb() => AppDatabase().getDatabase();
}
