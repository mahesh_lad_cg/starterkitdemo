import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:starter_kit_demo/utils/app_constant.dart';

class AppDatabase {
  static final AppDatabase database = AppDatabase._private();

  factory AppDatabase() => database;

  AppDatabase._private();

  Future<Database> getDatabase() async {
    final Directory path = await getApplicationDocumentsDirectory();
    final String dbPath = "${path.path}/${AppConstant.database.databaseName}.db";
    return openDatabase(
      dbPath,
      version: AppConstant.database.versionCode,
      onCreate: _onCreate,
      onUpgrade: (Database db, int oldVersion, int newVersion) async {
        final Batch batch = db.batch();
        if (oldVersion < newVersion) {
          await createAllTables(db);
          await alterTables(db);
        }
        await batch.commit();
      },
    );
  }

  Future<void> alterTables(Database db) async {}

  Future<void> _onCreate(Database db, int version) async {
    await createAllTables(db);
  }

  Future<void> createAllTables(Database db) async {}
}
