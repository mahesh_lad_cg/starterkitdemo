import 'package:dio/dio.dart';
import '../utils/app_constant.dart';
import '../configs/abstract_config.dart';
import 'dio_retry/options.dart';
import 'dio_retry/retry_interceptor.dart';
import 'error_interceptor.dart';
import 'network_client.dart';

class NetworkHandler {
  static NetworkHandler? _instance;
  final Dio _dio = Dio();
  final AbstractConfig _abstractConfig;

  factory NetworkHandler({
    required AbstractConfig abstractConfig,
    required bool isDebugMode,
  }) {
    _instance ??= NetworkHandler._internal(abstractConfig, isDebugMode);
    return _instance!;
  }

  NetworkHandler._internal(this._abstractConfig, bool isDebugMode) {
    configureClient(_dio, _abstractConfig);
    addRetryInterceptor(_dio);
    _dio.interceptors.add(ErrorInterceptor());
    addHeaderInterceptor(_dio);
  }

  ApiClient getClient() => DioClient(_dio, _abstractConfig);

  void configureClient(Dio dio, AbstractConfig abstractConfig) {
    dio.options.baseUrl = abstractConfig.baseURL;
    dio.options.connectTimeout = AppConstant.network.connectionTimeOut;
    dio.options.receiveTimeout = AppConstant.network.receiveTimeOut;
  }

  void addRetryInterceptor(Dio dioClient) {
    final RetryInterceptor retryInterceptor = RetryInterceptor(
      dio: _dio,
      options: RetryOptions(
        retries: 2, // Number of retries before a failure
        retryInterval:
            const Duration(seconds: 1), // Interval between each retry
        retryEvaluator: (DioError error) async {
          // if (checkTokenExpiry(error)) {
          //   try {
          //     await CurrentUserService.instance?.refreshToken();
          //   } catch (e) {
          //
          //     return false;
          //   }
          //   return true;
          // }
          return error.type != DioErrorType.cancel &&
              error.type != DioErrorType.response;
        }, // Evaluating if a retry is necessary regarding the error. It is a good candidate for updating authentication token in case of a unauthorized error (be careful with concurrency though)
      ),
    );
    dioClient.interceptors.add(retryInterceptor);
  }

  void addHeaderInterceptor(Dio dio) {
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (
          RequestOptions options,
          RequestInterceptorHandler handler,
        ) async {
          options = await HeaderInterceptor().intercept(options);
          return handler.next(options);
        },
      ),
    );
  }
}

class HeaderInterceptor {
  Future<RequestOptions> intercept(
    RequestOptions options,
  ) async {
    return options;
  }
}
