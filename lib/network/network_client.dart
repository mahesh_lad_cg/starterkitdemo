import 'package:dio/dio.dart';

import '../configs/abstract_config.dart';

class DioClient extends ApiClient {
  final Dio _dio;
  final AbstractConfig _abstractConfig;

  DioClient(this._dio, this._abstractConfig);

  @override
  Future<Response<T>> delete<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  }) async {
    final Options options = Options(headers: headers);
    _dio.options.baseUrl = _abstractConfig.baseURL;
    final Response<T> response = await _dio.delete(
      path = path,
      data: data,
      queryParameters: queryParameters,
      options: options,
    );
    return response;
  }

  @override
  Future<Response<T>> get<T>(
    String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers, {
    Options? options,
    String? baseUrl,
  }) async {
    if (baseUrl != null) {
      _dio.options.baseUrl = baseUrl;
    } else {
      _dio.options.baseUrl = _abstractConfig.baseURL;
    }
    final Response<T> response = await _dio.get(
      path = path,
      queryParameters: queryParameters,
      options: options,
    );
    return response;
  }

  @override
  Future<Response<T>> post<T>(
    String path, {
    dynamic data,
    Map<String, String>? headers,
    Options? options,
    String? baseUrl,
  }) async {
    options ??= Options(headers: headers);
    if (baseUrl != null) {
      _dio.options.baseUrl = baseUrl;
    } else {
      _dio.options.baseUrl = _abstractConfig.baseURL;
    }
    final Response<T> response =
        await _dio.post(path = path, data: data, options: options);
    return response;
  }

  @override
  Future<Response<T>> put<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  }) async {
    final Options options = Options(headers: headers);
    _dio.options.baseUrl = _abstractConfig.baseURL;
    final Response<T> response = await _dio.put(
      path = path,
      data: data,
      queryParameters: queryParameters,
      options: options,
    );
    return response;
  }
}

abstract class ApiClient {
  Future<Response<T>> get<T>(
    String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers, {
    Options? options,
    String? baseUrl,
  });

  Future<Response<T>> post<T>(
    String path, {
    dynamic data,
    Map<String, String>? headers,
    Options? options,
    String? baseUrl,
  });

  Future<Response<T>> put<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  });

  Future<Response<T>> delete<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? headers,
  });
}
